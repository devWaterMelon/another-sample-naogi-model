from argparse import ArgumentParser
import os
import sys
import numpy as np
from PIL import Image
from tensorflow.keras.preprocessing.image import ImageDataGenerator

#os.environ['CUDA_VISIBLE_DEVICES'] = '0'
#--------------------------------------------------------------------------------------------------------------

def get_sorted_files_in_folder(folder):
    files = [os.path.join(folder, f) for f in os.listdir(folder)]
    return sorted(files)
    

def parse_args():
    parser = ArgumentParser(description='Foot')
    
    parser.add_argument('--train_files_path')
    parser.add_argument('--test_files_path')
    parser.add_argument('--eval_files_path')
    parser.add_argument('--img_size', type=int, default=320, help='Image size')
    parser.add_argument('--batch_size', type=int, default=8, help='Batch size')
    parser.add_argument('--mode', required=True, choices=['train', 'eval', 'save_mobile'], help='Mode')
    parser.add_argument('--save_name', type=str, default='foot_best1', help='Name of save')
    
    return parser.parse_args()
    
    
args = parse_args()

SIZE = args.img_size
batch_size = args.batch_size
save_file = args.save_name + '.h5'

if args.mode == 'train':
    files = get_sorted_files_in_folder(args.train_files_path + '/ORIGINAL')
    mask_files = get_sorted_files_in_folder(args.train_files_path + '/MASK')

    test_files = get_sorted_files_in_folder(args.test_files_path + '/ORIGINAL')
    test_mask_files = get_sorted_files_in_folder(args.test_files_path + '/MASK')

if args.mode == 'eval':
    val_files = get_sorted_files_in_folder(args.eval_files_path)
    pred_path = 'predicted/'

    if not os.path.exists(pred_path):
        os.makedirs(pred_path)
#--------------------------------------------------------------------------------------------------------------

class Augment:
    def __init__(self):
        self.img_gen = ImageDataGenerator(width_shift_range=0.1,
                                          height_shift_range=0.1,
                                          rotation_range=30, 
                                          zoom_range=0.2, 
                                          horizontal_flip=True)
        
    def update(self):
        self.params = self.img_gen.get_random_transform((SIZE, SIZE))

        
    def apply(self, x):
        return self.img_gen.apply_transform(x=x, transform_parameters=self.params)
        
    
def image_to_np_array(img_filename):
    x = np.array(Image.open(img_filename).convert('RGB').resize((SIZE, SIZE), resample=Image.LANCZOS)).astype(np.float32)
    return x
    
    
def mask_to_np_array(img_filename):
    x = np.array(Image.open(img_filename).convert('RGB').resize((SIZE, SIZE), resample=Image.LANCZOS)).astype(np.float32)
    x = x.argmax(axis=-1).astype('int')
    x = np.eye(3, dtype=np.float32)[x]
    return x
    
    
def get_next_batch(files, mask_files, batch_size, train):
    index = np.arange(len(files))
    
    aug = Augment()
    
    while(True):
        in_batch = []
        out_batch = []
        i = 0

        np.random.shuffle(index)

        for n in index:
            if i == batch_size:
                in_batch = []
                out_batch = []
                i = 0
                
            aug.update()

            img = image_to_np_array(files[n])
            mask = mask_to_np_array(mask_files[n])
            
            if train == True:
                img = aug.apply(img)
                mask = aug.apply(mask)
            
            in_batch.append(img)
            out_batch.append(mask)
            
            i += 1

            if i < batch_size:
                continue
                
            in_batch = np.array(in_batch, dtype=np.float32)
            out_batch = np.array(out_batch, dtype=np.float32)
            yield in_batch, out_batch
        if i < batch_size:
            in_batch = np.array(in_batch, dtype=np.float32)
            out_batch = np.array(out_batch, dtype=np.float32)
            yield in_batch, out_batch
            
#--------------------------------------------------------------------------------------------------------------
##Model

import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.layers import Layer, Lambda, Input, Dense, BatchNormalization, AveragePooling2D, Conv2D, \
Reshape, Multiply, Add, UpSampling2D, RandomContrast, Permute, ZeroPadding2D, Rescaling
from tensorflow.keras.models import Model
from adabelief_tf import AdaBeliefOptimizer
import gctf

tf.config.optimizer.set_jit(True)

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)
        
        
def categorical_focal_loss(gamma=2., alpha=.25):
    """
    Softmax version of focal loss.
           m
      FL = ∑  -alpha * (1 - p_o,c)^gamma * y_o,c * log(p_o,c)
          c=1
      where m = number of classes, c = class and o = observation
    Parameters:
      alpha -- the same as weighing factor in balanced cross entropy
      gamma -- focusing parameter for modulating factor (1-p)
    Default value:
      gamma -- 2.0 as mentioned in the paper
      alpha -- 0.25 as mentioned in the paper
    References:
        Official paper: https://arxiv.org/pdf/1708.02002.pdf
        https://www.tensorflow.org/api_docs/python/tf/keras/backend/categorical_crossentropy
    Usage:
     model.compile(loss=[categorical_focal_loss(alpha=.25, gamma=2)], metrics=["accuracy"], optimizer=adam)
    """
    def categorical_focal_loss_fixed(y_true, y_pred):
        """
        :param y_true: A tensor of the same shape as `y_pred`
        :param y_pred: A tensor resulting from a softmax
        :return: Output tensor.
        """

        # Scale predictions so that the class probas of each sample sum to 1
        #y_pred /= K.sum(y_pred, axis=-1, keepdims=True)

        # Clip the prediction value to prevent NaN's and Inf's
        epsilon = K.epsilon()
        y_pred = K.clip(y_pred, epsilon, 1. - epsilon)

        # Calculate Cross Entropy
        cross_entropy = -y_true * K.log(y_pred)

        # Calculate Focal Loss
        loss = alpha * K.pow(1 - y_pred, gamma) * cross_entropy

        # Sum the losses in mini_batch
        return K.sum(loss, axis=1)

    return categorical_focal_loss_fixed
        
        
def dice_loss(y_true, y_pred):
    """dice loss function for tensorflow/keras
        calculate dice loss per batch and channel of each sample.
    Args:
        data_format: either channels_first or channels_last
    Returns:
        loss_function(y_true, y_pred)  
    """

    y_pred = tf.transpose(y_pred, (0, 3, 1, 2))
    y_true = tf.transpose(y_true, (0, 3, 1, 2))
        
    smooth = 1.0
    iflat = tf.reshape(y_pred, (tf.shape(y_pred)[0], tf.shape(y_pred)[1], -1))  # batch, channel, -1
    tflat = tf.reshape(y_true, (tf.shape(y_true)[0], tf.shape(y_true)[1], -1))
    intersection = K.sum(K.abs(iflat * tflat), axis=-1)
    
    return K.sum(1 - (2. * intersection + smooth) / (K.sum(K.square(iflat), axis=-1) + K.sum(K.square(tflat), axis=-1) + smooth), axis=1)


def DualLoss(y_true, y_pred):
    ce = K.mean(categorical_focal_loss()(y_true, y_pred))
    #ce = tf.keras.losses.CategoricalCrossentropy()(y_true, y_pred)
    dice = K.mean(dice_loss(y_true, y_pred))
    return ce + dice
        
        
class BatchWM(tf.keras.callbacks.Callback):
    def __init__(self, **kwargs):
        self.step = 1

    def on_batch_begin(self, batch, logs={}):
        lr = np.power(512, -0.5)*min(np.power(self.step, -0.5), self.step*np.power(8000, -1.5))
        K.set_value(self.model.optimizer.lr, lr)
        
        self.step += batch_size


def FTSwishG(threshold=-0.2):
    def _FTSwishG(x):
        return K.relu(x) * K.sigmoid(x*1.702) + threshold
    return Lambda(_FTSwishG)
#--------------------------------------------------------------------------------------------------------------

In = Input(shape=(SIZE, SIZE, 3))

x = RandomContrast(0.03)(In)

x = Rescaling(scale=1./127.5, offset=-1)(x)

base = tf.keras.applications.MobileNetV2(include_top=False, weights='imagenet')
sub_base1 = Model(inputs=base.input, outputs=base.layers[56].output) #/8
sub_base2 = Model(inputs=base.input, outputs=base.layers[118].output) #/16

x_skip = sub_base1(x)
x_skip = Conv2D(3, kernel_size=1)(x_skip)

x = sub_base2(x)

x1 = Conv2D(128, kernel_size=1, kernel_initializer='he_uniform')(x)
x1 = BatchNormalization()(x1)
x1 = FTSwishG()(x1)

x2 = AveragePooling2D(pool_size=4, strides=2)(x)
x2 = Conv2D(128, kernel_size=1, activation='sigmoid')(x2)
x2 = UpSampling2D(2, interpolation='bilinear')(x2)
x2 = ZeroPadding2D(padding=(1, 1))(x2)

x = Multiply()([x1, x2])

x = UpSampling2D(2, interpolation='bilinear')(x)
x = Conv2D(3, kernel_size=1)(x)

x = Add()([x, x_skip])

x = UpSampling2D(8, interpolation='bilinear')(x)

x = Conv2D(3, kernel_size=3, padding='same', activation='softmax')(x)

model = Model(inputs=In, outputs=x)
#--------------------------------------------------------------------------------------------------------------

optimizer = AdaBeliefOptimizer(lr=0.001, weight_decay=0.0001, print_change_log=False)
optimizer.get_gradients = gctf.centralized_gradients_for_optimizer(optimizer)

model.compile(optimizer=optimizer, loss=DualLoss, metrics=['acc'])
#--------------------------------------------------------------------------------------------------------------
    
if args.mode == 'train':
    ckp = tf.keras.callbacks.ModelCheckpoint(filepath=save_file, monitor='val_acc', save_best_only=True, save_weights_only=True, mode='max')
    lr_s = BatchWM()

    train_batch_iter = get_next_batch(files, mask_files, batch_size, train=True)
    test_batch_iter = get_next_batch(test_files, test_mask_files, batch_size, train=False)

    steps = len(files) // batch_size
    while steps * batch_size < len(files):
        steps += 1

    validation_steps = len(test_files) // batch_size
    while validation_steps * batch_size < len(test_files):
        validation_steps += 1

    model.fit(train_batch_iter, steps_per_epoch=steps, 
              validation_data=test_batch_iter, validation_steps=validation_steps, 
              batch_size=batch_size, epochs=100, callbacks=[ckp, lr_s])
#--------------------------------------------------------------------------------------------------------------

if not os.path.exists(save_file):
    print('No save file found!')
    sys.exit()
else:
    model.load_weights(args.save_file)

if args.mode == 'eval':
    for img_filename in val_files:
        img = Image.open(img_filename).convert('RGB')
        size = img.size
            
        img = np.array(img.resize((SIZE, SIZE), resample=Image.LANCZOS)).astype(np.float32)
        
        ans = model.predict(np.array([img]))[0]
        ans = ans.argmax(axis=-1).astype('int')
        mask = np.eye(3, dtype=np.uint8)[ans]
        mask *= 255
        mask = mask.astype(np.uint8)
        
        img = Image.fromarray(mask).resize((size[0], size[1]), resample=Image.LANCZOS)
        img = np.array(img)
        img = img.argmax(axis=-1).astype('int')
        img = np.eye(3, dtype=np.uint8)[img]
        img *= 255
        img = Image.fromarray(img.astype(np.uint8))
        img.save(pred_path + img_filename.split(os.sep)[-1].split('.')[0] + '.png')
#--------------------------------------------------------------------------------------------------------------

if args.mode == 'save_mobile':
    x = K.round(x)
    x *= 255
    x = Permute((3, 1, 2))(x)
    model = Model(inputs=In, outputs=x)
        
    import coremltools as ct
    import coremltools
    import coremltools.proto.FeatureTypes_pb2 as ft 


    name = '1.mlmodel'

    core_model = ct.convert(model, inputs=[ct.ImageType(shape=(1, SIZE, SIZE, 3))], source='tensorflow')
    core_model.save(name)
    #--------------------------------------------------------------------------------------------------------------

    spec = coremltools.utils.load_spec(name)
    spec.description.output[0].type.multiArrayType.shape.extend([1, 3, SIZE, SIZE]) #Добавляем шейп в явном виде

    updated_model = coremltools.models.MLModel(spec) #Сохраняем
    updated_model.save('2.mlmodel')
    #--------------------------------------------------------------------------------------------------------------

    spec = coremltools.utils.load_spec('2.mlmodel')
    print(spec.description)
    output = spec.description.output[0]
    del output.type.multiArrayType.shape[0] #Удаляем ось батча
    updated_model = coremltools.models.MLModel(spec) #Добавляем мета данные
    updated_model.author = 'Z union'
    updated_model.short_description = 'Image to mask'
    updated_model.version = '7'
    updated_model.save('4.mlmodel') #Сохраняем
    #--------------------------------------------------------------------------------------------------------------

    coreml_model = coremltools.models.MLModel('4.mlmodel') #И
    spec = coreml_model.get_spec()
    spec_layers = getattr(spec,spec.WhichOneof("Type")).layers

    # find the current output layer and save it for later reference
    last_layer = spec_layers[-1]
     
    # add the post-processing layer
    new_layer = spec_layers.add()
    new_layer.name = 'Identity'
     
    # Configure it as an activation layer
    new_layer.activation.linear.alpha = 255
    new_layer.activation.linear.beta = 0
     
    # Use the original model's output as input to this layer
    new_layer.input.append(last_layer.output[0])
     
    # Name the output for later reference when saving the model
    new_layer.output.append('image_output')
     
    # Find the original model's output description
    output_description = next(x for x in spec.description.output if x.name==last_layer.output[0])
     
    # Update it to use the new layer as output
    output_description.name = new_layer.name

    def convert_multiarray_output_to_image(spec, feature_name, is_bgr=False):
        """ 
        Convert an output multiarray to be represented as an image 
        This will modify the Model_pb spec passed in. 
        Example: 
            model = coremltools.models.MLModel('MyNeuralNetwork.mlmodel') 
            spec = model.get_spec() 
            convert_multiarray_output_to_image(spec,'imageOutput',is_bgr=False) 
            newModel = coremltools.models.MLModel(spec) 
            newModel.save('MyNeuralNetworkWithImageOutput.mlmodel') 
        Parameters 
        ---------- 
        spec: Model_pb 
            The specification containing the output feature to convert 
        feature_name: str 
            The name of the multiarray output feature you want to convert 
        is_bgr: boolean 
            If multiarray has 3 channels, set to True for RGB pixel order or false for BGR 
        """
        for output in spec.description.output: 
            if output.name != feature_name: 
                continue
            if output.type.WhichOneof('Type') != 'multiArrayType': 
                raise ValueError("%s is not a multiarray type" % output.name) 
            array_shape = tuple(output.type.multiArrayType.shape) 
            print(array_shape)
            channels, height, width  = array_shape 
            from coremltools.proto import FeatureTypes_pb2 as ft 
            if channels == 1: 
                output.type.imageType.colorSpace = ft.ImageFeatureType.ColorSpace.Value('GRAYSCALE') 
            elif channels == 3: 
                if is_bgr: 
                    output.type.imageType.colorSpace = ft.ImageFeatureType.ColorSpace.Value('BGR') 
                else: 
                    output.type.imageType.colorSpace = ft.ImageFeatureType.ColorSpace.Value('RGB') 
            else: 
                raise ValueError("Channel Value %d not supported for image inputs" % channels) 
            output.type.imageType.width = width 
            output.type.imageType.height = height
            print(output.type.imageType)
     
    # Mark the new layer as image
    convert_multiarray_output_to_image(spec, output_description.name, is_bgr=False)

    updated_model = coremltools.models.MLModel(spec) #Сохраняем с ImageType на конце
    updated_model.save('5.mlmodel')

    spec = coremltools.utils.load_spec('5.mlmodel')
    coremltools.utils.rename_feature(spec, 'img', 'input_image') #Меняем названия слоев
    coremltools.utils.rename_feature(spec, 'Identity', 'output_mask')

    updated_model = coremltools.models.MLModel(spec) #Конечный файл
    updated_model.save(args.save_name + '.mlmodel')
    #--------------------------------------------------------------------------------------------------------------
    
    os.remove('1.mlmodel')
    os.remove('2.mlmodel')
    os.remove('4.mlmodel')
    os.remove('5.mlmodel')















