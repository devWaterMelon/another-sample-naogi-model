# Try.Fit
Foot mask prediction


## Использование
`python Foot.py --train_files_path PATH_TO_TRAIN_DATASET --test_files_path PATH_TO_TEST_DATASET --eval_files_path PATH_TO_EVAL_FILES --img_size 320 --batch_size 8 --mode MODE --save_name foot_best1`

Dataset's folders must include `MASK` and `ORIGINAL` subfolders.

MODE:

`train` - запуск обучения модели

`eval` - обработать фото из папки и сохранить результаты в папку ./predicted

`save_mobile` - сохранение обученной .h5 модели в формат .mlmodel

      

        



