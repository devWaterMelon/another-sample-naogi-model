from naogi import NaogiModel, FileRenderer

# import os
# import sys
import io
import numpy as np
from PIL import Image

import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.layers import Layer, Lambda, Input, Dense, BatchNormalization, AveragePooling2D, Conv2D, \
Reshape, Multiply, Add, UpSampling2D, Permute, ZeroPadding2D, RandomContrast, Rescaling
from tensorflow.keras.models import Model as KerasModel
from adabelief_tf import AdaBeliefOptimizer
# import gctf #pip gradient-centralization-tf


class Model(NaogiModel):

  SIZE = 320

  model_h5 = '/app/model/foot_best1.h5'

  def predict(self):
    img = Image.open(self.image).convert('RGB')
    size = img.size

    img = np.array(img.resize((self.SIZE, self.SIZE), resample=Image.LANCZOS)).astype(np.float32)

    ans = self.model.predict(np.array([img]))[0]
    ans = ans.argmax(axis=-1).astype('int')
    mask = np.eye(3, dtype=np.uint8)[ans]
    mask *= 255
    mask = mask.astype(np.uint8)

    img = Image.fromarray(mask).resize((size[0], size[1]), resample=Image.LANCZOS)
    img = np.array(img)
    img = img.argmax(axis=-1).astype('int')
    img = np.eye(3, dtype=np.uint8)[img]
    img *= 255

    img = Image.fromarray(img.astype(np.uint8))

    object_io = io.BytesIO()
    img.save(object_io, 'PNG')
    object_io.seek(0)
    return object_io
    # return Image.fromarray(img.astype(np.uint8))
    # img.save(pred_path + img_filename.split(os.sep)[-1].split('.')[0] + '.png')

  def load_model(self):
    tf.config.optimizer.set_jit(True)

    gpus = tf.config.experimental.list_physical_devices('GPU')

    if gpus:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)

    self.model = self._get_model()
    self.model.load_weights(self.model_h5)

  def prepare(self, params_dict):
    self.image = params_dict['file']

  def renderer(self):
    return FileRenderer


  def _get_model(self):
    def DualLoss(y_true, y_pred):
      ce = K.mean(categorical_focal_loss()(y_true, y_pred))
      #ce = tf.keras.losses.CategoricalCrossentropy()(y_true, y_pred)
      dice = K.mean(dice_loss(y_true, y_pred))
      return ce + dice

    def FTSwishG(threshold=-0.2):
      def _FTSwishG(x):
        return K.relu(x) * K.sigmoid(x*1.702) + threshold
      return Lambda(_FTSwishG)

    In = Input(shape=(self.SIZE, self.SIZE, 3))

    x = RandomContrast(0.03)(In)
    x = Rescaling(scale=1./127.5, offset=-1)(x)

    base = tf.keras.applications.MobileNetV2(include_top=False, weights='imagenet')
    sub_base1 = KerasModel(inputs=base.input, outputs=base.layers[56].output) #/8
    sub_base2 = KerasModel(inputs=base.input, outputs=base.layers[118].output) #/16

    x_skip = sub_base1(x)
    x_skip = Conv2D(3, kernel_size=1)(x_skip)

    x = sub_base2(x)

    x1 = Conv2D(128, kernel_size=1, kernel_initializer='he_uniform')(x)
    x1 = BatchNormalization()(x1)
    x1 = FTSwishG()(x1)

    x2 = AveragePooling2D(pool_size=4, strides=2)(x)
    x2 = Conv2D(128, kernel_size=1, activation='sigmoid')(x2)
    x2 = UpSampling2D(2, interpolation='bilinear')(x2)
    x2 = ZeroPadding2D(padding=(1, 1))(x2)

    x = Multiply()([x1, x2])

    x = UpSampling2D(2, interpolation='bilinear')(x)
    x = Conv2D(3, kernel_size=1)(x)

    x = Add()([x, x_skip])

    x = UpSampling2D(8, interpolation='bilinear')(x)

    x = Conv2D(3, kernel_size=3, padding='same', activation='softmax')(x)

    model = KerasModel(inputs=In, outputs=x)

    optimizer = AdaBeliefOptimizer(lr=0.001, weight_decay=0.0001, print_change_log=False)
    # optimizer.get_gradients = gctf.centralized_gradients_for_optimizer(optimizer)

    model.compile(optimizer=optimizer, loss=DualLoss, metrics=['acc'])

    return model
